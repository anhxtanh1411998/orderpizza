package com.studentproject.orderpizza.utils;

import com.studentproject.orderpizza.R;
import com.studentproject.orderpizza.api.RetrofitClient;
import com.studentproject.orderpizza.api.SOService;

public class ApiUtils {

    public static final String BASE_URL = "https://orderpizza-node-api.herokuapp.com/";

    public static SOService getSOService() {
        return RetrofitClient.getClient(BASE_URL).create(SOService.class);
    }
}
