package com.studentproject.orderpizza.utils;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

import com.studentproject.orderpizza.MainActivity;

public class ShowToast {
    private void ShowToast(MainActivity getActivity, String message) {
        getActivity.runOnUiThread(() -> {
            Toast.makeText(getActivity, message, Toast.LENGTH_SHORT).show();
        });
    }

    private void ShowToast(MainActivity getActivity, String message, int gravity, int x, int y) {
        getActivity.runOnUiThread(() -> {
            Toast toast = Toast.makeText(getActivity, message, Toast.LENGTH_SHORT);
            toast.setGravity(gravity, x, y);
            toast.show();
        });
    }
}
