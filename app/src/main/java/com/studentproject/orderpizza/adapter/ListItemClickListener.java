package com.studentproject.orderpizza.adapter;

public interface ListItemClickListener {
    void onListItemClick(int position, boolean isLongClick);
}
