package com.studentproject.orderpizza.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.studentproject.orderpizza.R;
import com.studentproject.orderpizza.model.PizzaOrder;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ListCartAdapter extends RecyclerView.Adapter<ListCartAdapter.ViewHolder> {
    private final ArrayList<PizzaOrder> mPizzaOrders;
    private final Context mContext;
    private final ListItemClickListener mOnClickListener;

    public ListCartAdapter(Context context, ArrayList<PizzaOrder> pizzaOrders, ListItemClickListener listItemClickListener) {
        this.mPizzaOrders = pizzaOrders;
        this.mContext = context;
        this.mOnClickListener = listItemClickListener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.item_cart, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Picasso.get()
                .load(mPizzaOrders.get(position).getLinkImage())
                .placeholder(R.drawable.ic_launcher_background)
                .into(holder.imageViewPizza);
        holder.textViewName.setText(mPizzaOrders.get(position).getName());
        holder.textViewPrice.setText("Giá: " + mPizzaOrders.get(position).getTotalPrice() + "đ");
        holder.textViewIngredient.setText(mPizzaOrders.get(position).getIngredient());
        holder.textViewAmount.setText("Số lượng: " + mPizzaOrders.get(position).getAmount());
        holder.textViewStatus.setText(mPizzaOrders.get(position).getStatusOrder());
    }

    @Override
    public int getItemCount() {
        return mPizzaOrders.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private final ImageView imageViewPizza;
        private final TextView textViewPrice, textViewIngredient, textViewName, textViewAmount, textViewStatus;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.textViewNamePizza);
            imageViewPizza = itemView.findViewById(R.id.imageViewPizza);
            textViewPrice = itemView.findViewById(R.id.textViewPrice);
            textViewIngredient = itemView.findViewById(R.id.textViewIngredient);
            textViewAmount = itemView.findViewById(R.id.textViewAmount);
            textViewStatus = itemView.findViewById(R.id.textViewStatus);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            mOnClickListener.onListItemClick(position, false);
        }

        @Override
        public boolean onLongClick(View v) {
            mOnClickListener.onListItemClick(getAdapterPosition(),true);
            return true;
        }
    }

}
