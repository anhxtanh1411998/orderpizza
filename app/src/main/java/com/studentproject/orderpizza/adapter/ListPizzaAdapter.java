package com.studentproject.orderpizza.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.studentproject.orderpizza.R;
import com.studentproject.orderpizza.model.Pizza;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ListPizzaAdapter extends RecyclerView.Adapter<ListPizzaAdapter.ViewHolder> {
    private final ArrayList<Pizza> mPizzas;
    private final Context mContext;
    private final ListItemClickListener mOnClickListener;

    public ListPizzaAdapter(Context context, ArrayList<Pizza> pizzas, ListItemClickListener listItemClickListener) {
        this.mPizzas = pizzas;
        this.mContext = context;
        this.mOnClickListener = listItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.item_pizza, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Picasso.get()
                .load(mPizzas.get(position).getLink_image())
                .placeholder(R.drawable.ic_launcher_background)
                .into(holder.imageViewPizza);
        holder.textViewName.setText(mPizzas.get(position).getName());
        holder.textViewPrice.setText("Giá: " + mPizzas.get(position).getPrice() + "đ");
        holder.textViewIngredient.setText(mPizzas.get(position).getIngredient());
    }

    @Override
    public int getItemCount() {
        return mPizzas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ImageView imageViewPizza;
        private final TextView textViewPrice, textViewIngredient, textViewName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.textViewName);
            imageViewPizza = itemView.findViewById(R.id.imageViewPizza);
            textViewPrice = itemView.findViewById(R.id.textViewPrice);
            textViewIngredient = itemView.findViewById(R.id.textViewIngredient);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            mOnClickListener.onListItemClick(position, false);
        }
    }
}
