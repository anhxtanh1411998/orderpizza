package com.studentproject.orderpizza.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.studentproject.orderpizza.R;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ListMessageAdapter extends RecyclerView.Adapter<ListMessageAdapter.ViewHolder> {
    private final ArrayList<String> mMessages;
    private final Context mContext;

    public ListMessageAdapter(Context context, ArrayList<String> messages) {
        this.mContext = context;
        this.mMessages = messages;
    }

    @NonNull
    @Override
    public ListMessageAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_message, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListMessageAdapter.ViewHolder holder, int position) {
        if (mMessages.get(position).contains("admin:")&& !mMessages.get(position).equals("")) {
            holder.textViewMessage2.setText(mMessages.get(position));
            holder.textViewMessage1.setVisibility(View.GONE);
        }
        else if(!mMessages.get(position).equals("")){
            holder.textViewMessage2.setVisibility(View.GONE);
            holder.textViewMessage1.setText(mMessages.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return mMessages.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView textViewMessage1, textViewMessage2;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewMessage1 = itemView.findViewById(R.id.textViewMessage1);
            textViewMessage2 = itemView.findViewById(R.id.textViewMessage2);

        }
    }
}
