package com.studentproject.orderpizza;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import de.hdodenhof.circleimageview.CircleImageView;
import io.socket.client.IO;
import io.socket.client.Socket;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Picasso;
import com.studentproject.orderpizza.model.PizzaOrder;
import com.studentproject.orderpizza.model.User;
import com.studentproject.orderpizza.view.fragment.CartFragment;
import com.studentproject.orderpizza.view.fragment.ListPizzaFragment;
import com.studentproject.orderpizza.view.fragment.LoginFragment;
import com.studentproject.orderpizza.view.fragment.MessageFragment;

import org.json.JSONArray;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, LoginFragment.SendData {

    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("https://orderpizza-node-api.herokuapp.com/");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
    private ArrayList<PizzaOrder> mPizzaOrderList;
    public static User mUser = null;
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createView();
        updateStatus();
        mNavView.getHeaderView(0).setVisibility(View.GONE);
    }

    private void loadFragment(Fragment frameLayout, Boolean addToBackStack) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        if (addToBackStack) {
            fragmentTransaction.replace(R.id.frameLayout, frameLayout).addToBackStack(null);
        } else {
            fragmentTransaction.replace(R.id.frameLayout, frameLayout);
        }
        fragmentTransaction.commit(); // save the changes
    }

    @SuppressLint("ResourceType")
    private void createView() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        mDrawerLayout = findViewById(R.id.drawerLayout);
        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_logo_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mNavView = findViewById(R.id.nav_view);
        mNavView.setNavigationItemSelectedListener(this);

        ImageView imgViewCart = findViewById(R.id.imageViewCart);
        imgViewCart.setOnClickListener(v -> handleClickCart());

        ImageView imgMessage = findViewById(R.id.imageViewMessage);
        imgMessage.setOnClickListener(v -> loadFragment(new MessageFragment(), true));

        loadFragment(new ListPizzaFragment(), false);
    }
    private void handleClickCart(){
        if (mUser == null) {
            handleNotUser();
        } else getListCart();
    }

    private void handleNotUser() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Thông báo");
        builder.setMessage("Bạn phải đăng nhập để thực hiện chức năng này!");
        builder.setPositiveButton("Đồng ý", (dialog, which) -> {
            loadFragment(new LoginFragment(), true);
        });
        builder.setNegativeButton("Hủy", (dialog, which) -> {
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            mDrawerLayout.openDrawer(GravityCompat.START);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cart:
                handleClickCart();
                break;
            case R.id.action_setting:
                Toast.makeText(this, "Action setting", Toast.LENGTH_LONG).show();
                break;
            case R.id.action_logout:
                mUser = null;
                setDataNavHeader(mUser);
                Toast.makeText(this, "Đã đăng xuất!", Toast.LENGTH_LONG).show();
                setVisibleLoginLogOut(true, false);
                break;
            case R.id.action_login:
                loadFragment(new LoginFragment(), true);
                break;
        }
        //item.setChecked(true);
        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setVisibleLoginLogOut(boolean login, boolean logout) {
        mNavView.getMenu().findItem(R.id.action_login).setVisible(login);
        mNavView.getMenu().findItem(R.id.action_logout).setVisible(logout);
    }

    private void setDataNavHeader(User user) {
        CircleImageView imgAvatar = findViewById(R.id.nav_header_imageView);
        TextView tvName = findViewById(R.id.nav_header_textView);
        if (user != null) {
            mNavView.getHeaderView(0).setVisibility(View.VISIBLE);
            Picasso.get().load(user.getLinkImage()).into(imgAvatar);
            tvName.setText(user.getName());
            setVisibleLoginLogOut(false, true);
        } else {
            mNavView.getHeaderView(0).setVisibility(View.GONE);
        }
    }

    @Override
    public void imLeavingYou(User user) {
        mUser = user;
        if (mUser != null) {
            Toast.makeText(this, "Đăng nhập thành công!", Toast.LENGTH_SHORT).show();
            setDataNavHeader(mUser);
        }
    }

    public static User getmUser() {
        return mUser;
    }

    private void getListCart() {
        mPizzaOrderList = new ArrayList<>();
        mSocket.connect();
        mSocket.emit("get_cart", mUser.getId());
        mSocket.on("list_cart", args -> {
            mPizzaOrderList.clear();
            JSONArray jsonArray = (JSONArray) args[0];
            if (jsonArray == null)
                this.runOnUiThread(() -> {
                    Toast toast = Toast.makeText(this, "Giỏ hàng trống", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                });
            else {
                mPizzaOrderList = PizzaOrder.fromJson(jsonArray);
                CartFragment cartFragment = new CartFragment();
                Bundle args1 = new Bundle();
                args1.putParcelableArrayList("cart", mPizzaOrderList);
                cartFragment.setArguments(args1);
                loadFragment(cartFragment, true);
            }
        });
    }
    private void updateStatus() {
        mSocket.connect();
        if (mUser != null) {
            mSocket.on("status_update_order", args -> {
                String statusUpdate = (String) args[0];
                if (statusUpdate.equals("success")) {
                    this.runOnUiThread(this::getListCart);
                }

            });
        }
    }


}