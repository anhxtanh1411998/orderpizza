package com.studentproject.orderpizza.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import androidx.annotation.NonNull;

public class User implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("role")
    @Expose
    private int role;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("password")
    @Expose
    private String password;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("phone")
    @Expose
    private String phone;

    @SerializedName("age")
    @Expose
    private String age;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("link_image")
    @Expose
    private String linkImage;

    public User(){}

    public User(int role, String username, String password) {
        this.role = role;
        this.username = username;
        this.password = password;
    }

    public User(int role, String name, String username, String password, String address, String phone){
        this.role = role;
        this.name = name;
        this.username = username;
        this.password = password;
        this.address = address;
        this.phone = phone;
    }

    public User(String username, String password, String name, String address) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLinkImage() {
        return linkImage;
    }

    public void setLinkImage(String linkImage) {
        this.linkImage = linkImage;
    }

    public static Creator<User> getCREATOR() {
        return CREATOR;
    }

    protected User(Parcel in) {
        id = in.readInt();
        role = in.readInt();
        username = in.readString();
        password = in.readString();
        name = in.readString();
        phone = in.readString();
        age = in.readString();
        address = in.readString();
        linkImage = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(role);
        dest.writeString(username);
        dest.writeString(password);
        dest.writeString(name);
        dest.writeString(phone);
        dest.writeString(age);
        dest.writeString(address);
        dest.writeString(linkImage);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public static User fromJson(JSONObject jsonObject){
        User user = new User();
        try {
            user.id = jsonObject.getInt("id");
            user.role = jsonObject.getInt("role");
            user.username =jsonObject.getString("username");
            user.password =jsonObject.getString("password");
            user.name =jsonObject.getString("name");
            user.phone =jsonObject.getString("phone");
            user.age =jsonObject.getString("age");
            user.address =jsonObject.getString("address");
            user.linkImage =jsonObject.getString("link_image");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return user;
    }

    @NonNull
    @Override
    public String toString() {
        String user = "user: {" +
                "id:" + id
                + ",role:" + role
                + ",username:" + username
                + ",password:" + password
                + ",name:" + name
                + ",phone:" + phone
                + ",age:" + age
                + ",address:" + address
                + ",link_image:" + linkImage
                + "}";
        return user;
    }

}
