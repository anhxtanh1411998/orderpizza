package com.studentproject.orderpizza.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.annotation.NonNull;

public class Pizza implements Parcelable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("link_image")
    @Expose
    private String link_image;
    @SerializedName("ingredient")
    @Expose
    private String ingredient;

    public Pizza(Integer id, String name, String price, String amount, String link_image, String ingredient) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.amount = amount;
        this.link_image = link_image;
        this.ingredient = ingredient;
    }

    public Pizza(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getLink_image() {
        return link_image;
    }

    public void setLink_image(String link_image) {
        this.link_image = link_image;
    }

    public String getIngredient() {
        return ingredient;
    }

    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }

    public static Creator<Pizza> getCREATOR() {
        return CREATOR;
    }

    protected Pizza(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        name = in.readString();
        price = in.readString();
        amount = in.readString();
        link_image = in.readString();
        ingredient = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(name);
        dest.writeString(price);
        dest.writeString(amount);
        dest.writeString(link_image);
        dest.writeString(ingredient);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Pizza> CREATOR = new Creator<Pizza>() {
        @Override
        public Pizza createFromParcel(Parcel in) {
            return new Pizza(in);
        }

        @Override
        public Pizza[] newArray(int size) {
            return new Pizza[size];
        }
    };

    @NonNull
    @Override
    public String toString() {
        String pizza = "pizza: {" +
                "id:" + id
                + ",name:" + name
                + ",price:" + price
                + ",amount:" + amount
                + ",link_image:" + link_image
                + ",ingredient:" + ingredient
                + "}";
        return pizza;
    }
}
