package com.studentproject.orderpizza.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PizzaOrder implements Parcelable {
    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("ingredient")
    @Expose
    private String ingredient;

    @SerializedName("link_image")
    @Expose
    private String linkImage;

    @SerializedName("amount")
    @Expose
    private int amount;

    @SerializedName("total_price")
    @Expose
    private String totalPrice;

    @SerializedName("status_order")
    @Expose
    private String statusOrder;


    public PizzaOrder(String name, String ingredient, String linkImage, int amount, String totalPrice, String statusOrder) {
        this.name = name;
        this.ingredient = ingredient;
        this.linkImage = linkImage;
        this.amount = amount;
        this.totalPrice = totalPrice;
        this.statusOrder = statusOrder;
    }

    public PizzaOrder(int id, String name, String ingredient, String linkImage, int amount, String totalPrice, String statusOrder) {
        this.id = id;
        this.name = name;
        this.ingredient = ingredient;
        this.linkImage = linkImage;
        this.amount = amount;
        this.totalPrice = totalPrice;
        this.statusOrder = statusOrder;
    }

    public PizzaOrder(){}

    protected PizzaOrder(Parcel in) {
        id = in.readInt();
        name = in.readString();
        ingredient = in.readString();
        linkImage = in.readString();
        amount = in.readInt();
        totalPrice = in.readString();
        statusOrder = in.readString();
    }

    public static final Creator<PizzaOrder> CREATOR = new Creator<PizzaOrder>() {
        @Override
        public PizzaOrder createFromParcel(Parcel in) {
            return new PizzaOrder(in);
        }

        @Override
        public PizzaOrder[] newArray(int size) {
            return new PizzaOrder[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(ingredient);
        dest.writeString(linkImage);
        dest.writeInt(amount);
        dest.writeString(totalPrice);
        dest.writeString(statusOrder);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIngredient() {
        return ingredient;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }

    public String getLinkImage() {
        return linkImage;
    }

    public void setLinkImage(String linkImage) {
        this.linkImage = linkImage;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getStatusOrder() {
        return statusOrder;
    }

    public void setStatusOrder(String statusOrder) {
        this.statusOrder = statusOrder;
    }

    public static Creator<PizzaOrder> getCREATOR() {
        return CREATOR;
    }

    public static PizzaOrder fromJson(JSONObject jsonObject) {
        PizzaOrder pizzaOrder = new PizzaOrder();
        // Deserialize json into object fields
        try {
            pizzaOrder.id = jsonObject.getInt("id");
            pizzaOrder.name = jsonObject.getString("name");
            pizzaOrder.ingredient = jsonObject.getString("ingredient");
            pizzaOrder.linkImage = jsonObject.getString("link_image");
            pizzaOrder.amount = jsonObject.getInt("amount");
            pizzaOrder.totalPrice = jsonObject.getString("total_price");
            pizzaOrder.statusOrder = jsonObject.getString("status_order");

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        // Return new object
        return pizzaOrder;
    }

    public static ArrayList<PizzaOrder> fromJson(JSONArray jsonArray) {
        JSONObject pizzaOrderJson;
        ArrayList<PizzaOrder> pizzaOrders = new ArrayList<>();
        // Process each result in json array, decode and convert to business object
        for (int i=0; i < jsonArray.length(); i++) {
            try {
                pizzaOrderJson = jsonArray.getJSONObject(i);
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }

            PizzaOrder pizzaOrder = PizzaOrder.fromJson(pizzaOrderJson);
            if (pizzaOrder != null) {
                pizzaOrders.add(pizzaOrder);
            }
        }

        return pizzaOrders;
    }
}
