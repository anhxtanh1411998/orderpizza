package com.studentproject.orderpizza.api;

import com.studentproject.orderpizza.model.Pizza;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface SOService {

    @GET("pizza")
    Call<ArrayList<Pizza>> getPizza();
}

