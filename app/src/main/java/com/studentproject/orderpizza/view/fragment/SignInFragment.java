package com.studentproject.orderpizza.view.fragment;

import android.app.Activity;
import android.os.Bundle;

import android.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.studentproject.orderpizza.MainActivity;
import com.studentproject.orderpizza.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;


public class SignInFragment extends Fragment {

    private MainActivity mActivity;
    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("https://orderpizza-node-api.herokuapp.com/");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }


    public SignInFragment() {
    }

    public static SignInFragment newInstance(String param1, String param2) {
        SignInFragment fragment = new SignInFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) this.getActivity();
        mSocket.connect();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_in, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        EditText edtName = view.findViewById(R.id.editTextName);

        EditText edtUserName = view.findViewById(R.id.editTextUser);

        EditText edtPassword = view.findViewById(R.id.editTextPassword);

        EditText edtAddress = view.findViewById(R.id.editTextAddress);

        EditText edtRePassword = view.findViewById(R.id.edtTextRePassword);

        EditText edtPhone = view.findViewById(R.id.editTextPhone);

        ImageButton imgBtBack =  view.findViewById(R.id.imageButtonBack);
        Button btSignUp = view.findViewById(R.id.buttonSignIn);
        TextView tvLogin = view.findViewById(R.id.textViewLogin);

        setEventClick(edtName, edtUserName, edtPassword, edtRePassword, edtAddress, edtPhone, imgBtBack, btSignUp, tvLogin);
    }

    private void setEventClick(EditText edtName, EditText edtUserName, EditText edtPassword, EditText edtRePassword, EditText edtAddress, EditText edtPhone,
                               ImageButton imgBtBack, Button btSignUp, TextView tvLogin) {
        btSignUp.setOnClickListener(v -> {
            String name = edtName.getText().toString();
            String userName = edtUserName.getText().toString();
            String password = edtPassword.getText().toString();
            String address = edtAddress.getText().toString();
            String rePassword = edtRePassword.getText().toString();
            String phone = edtPhone.getText().toString();
            if (checkConditionEditText(name, userName, password, address, rePassword, phone)) {
                requestSignUp(name, userName, password, phone, address);
                checkSignUp();
            }
        });

        tvLogin.setOnClickListener(v -> {
            getActivity().getFragmentManager().popBackStack();
        });

        imgBtBack.setOnClickListener(v -> {
            mActivity.getFragmentManager().popBackStack();
        });
    }

    private boolean checkConditionEditText(String name, String userName, String password, String address, String rePassword, String phone) {
        if (!password.equals(rePassword)) {
            Toast.makeText(mActivity, "Mật khẩu không khớp!", Toast.LENGTH_SHORT).show();
            return false;
        } else if (name.equals("") || userName.equals("") || password.equals("")
                || address.equals("") || phone.equals("")) {
            Toast.makeText(mActivity, "Nhập đầy đủ thông tin!", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void requestSignUp(String name, String userName, String password, String phone, String address) {
        JSONObject newUser = new JSONObject();
        try {
            newUser.put("name", name);
            newUser.put("username", userName);
            newUser.put("password", password);
            newUser.put("phone", phone);
            newUser.put("address", address);
            mSocket.emit("sign_in", newUser);

        } catch (JSONException e) {
            e.getMessage();
        }
    }

    private void checkSignUp() {
        mSocket.on("sign_up_success", args -> {
            String object = (String) args[0];
            if(object.equals("username available")){
                mActivity.runOnUiThread(() ->
                        Toast.makeText(mActivity, "Tên đăng nhập đã tồn tại!", Toast.LENGTH_SHORT).show());
            }
            else if(object.equals("success")){
                mActivity.runOnUiThread(() ->
                        Toast.makeText(getActivity(), "Đăng ký thành công!", Toast.LENGTH_SHORT).show());
                mActivity.getFragmentManager().popBackStack();
            }
            else {
                mActivity.runOnUiThread(() ->
                        Toast.makeText(mActivity, "Đăng ký thất bại!", Toast.LENGTH_SHORT).show());
            }
        });

    }


}