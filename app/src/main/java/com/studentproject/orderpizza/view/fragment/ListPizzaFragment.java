package com.studentproject.orderpizza.view.fragment;

import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.Bundle;

import android.app.Fragment;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.studentproject.orderpizza.MainActivity;
import com.studentproject.orderpizza.R;
import com.studentproject.orderpizza.adapter.ListItemClickListener;
import com.studentproject.orderpizza.adapter.ListPizzaAdapter;
import com.studentproject.orderpizza.model.Pizza;
import com.studentproject.orderpizza.model.User;
import com.studentproject.orderpizza.utils.ApiUtils;
import com.studentproject.orderpizza.api.SOService;

import java.util.ArrayList;
import java.util.Objects;

public class ListPizzaFragment extends Fragment implements ListItemClickListener {

    private static final String PIZZA = "pizza";
    private static final String USER = "user";

    private Dialog dialog;
    private final ArrayList<Pizza> mPizza;
    private ListPizzaAdapter mAdapter;
    private MainActivity mActivity;

    private User mUser;


    public ListPizzaFragment() {
        mPizza = new ArrayList<>();
    }

    public static ListPizzaFragment newInstance(ArrayList<Pizza> mPizza) {
        ListPizzaFragment fragment = new ListPizzaFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList("pizza", mPizza);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mUser = getArguments().getParcelable(USER);
        }
        mActivity = (MainActivity) this.getActivity();
        getListPizza();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_pizza, container, false);
        initRecycleView(view);
        return view;
    }

    public void getListPizza() {
        dialog = ProgressDialog.show(getContext(), "Loading...", "Please wait...", true);

        SOService mAPIService = ApiUtils.getSOService();
        Call<ArrayList<Pizza>> getPizza = mAPIService.getPizza();
        getPizza.enqueue(new Callback<ArrayList<Pizza>>() {
            @Override
            public void onResponse(Call<ArrayList<Pizza>> call, Response<ArrayList<Pizza>> response) {
                mPizza.clear();
                mPizza.addAll(response.body());
                mAdapter.notifyDataSetChanged();
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<ArrayList<Pizza>> call, Throwable t) {
                dialog.dismiss();
                Log.e("TAG", Objects.requireNonNull(t.getMessage()));
            }
        });
    }

    @Override
    public void onListItemClick(int position, boolean isLongClick) {
        loadFragment(new OrderPizzaFragment(), position);
    }



    private void loadFragment(Fragment frameLayout, int position) {
        Bundle args = new Bundle();
        args.putParcelable(PIZZA, mPizza.get(position));
        args.putParcelable(USER, mUser);
        frameLayout.setArguments(args);
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, frameLayout).addToBackStack(null);
        fragmentTransaction.commit(); // save the changes
    }

    private void initRecycleView(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.recycleViewListPizza);
        mAdapter = new ListPizzaAdapter(mActivity, mPizza, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setAdapter(mAdapter);
    }
}