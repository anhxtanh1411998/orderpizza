package com.studentproject.orderpizza.view.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import android.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.studentproject.orderpizza.MainActivity;
import com.studentproject.orderpizza.R;
import com.studentproject.orderpizza.model.Pizza;
import com.studentproject.orderpizza.model.PizzaOrder;
import com.studentproject.orderpizza.model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.UUID;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class OrderPizzaFragment extends Fragment {

    private static final String PIZZA = "pizza";

    private Socket mSocket;

    {
        try {
            mSocket = IO.socket("https://orderpizza-node-api.herokuapp.com/");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private MainActivity mActivity;
    private int count = 1;
    private Pizza mPizza;
    private User mUser;
    private TextView mTvPrice, mTvCount;

    public OrderPizzaFragment() {
    }

    public static OrderPizzaFragment newInstance(Pizza pizza) {
        OrderPizzaFragment fragment = new OrderPizzaFragment();
        Bundle args = new Bundle();
        args.putParcelable(PIZZA, pizza);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSocket.connect();
        mActivity = (MainActivity) this.getActivity();
        if (getArguments() != null) {
            mPizza = getArguments().getParcelable(PIZZA);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_pizza, container, false);
        this.mUser = MainActivity.getmUser();
        Log.e("order fragmnet", String.valueOf(MainActivity.mUser));
        initView(view);
        return view;
    }

    @SuppressLint("SetTextI18n")
    private void initView(View view) {

        // load image pizza
        ImageView imgPizza = view.findViewById(R.id.imageViewPizza);
        Picasso.get()
                .load(mPizza.getLink_image())
                .placeholder(R.drawable.ic_launcher_background)
                .into(imgPizza);

        // set name pizza
        TextView tvNamePizza = view.findViewById(R.id.textViewNamePizza);
        tvNamePizza.setText(mPizza.getName());

        // set ingredient of pizza
        TextView tvIngredient = view.findViewById(R.id.textViewIngredient);
        tvIngredient.setText(mPizza.getIngredient());

        // set price pizza
        mTvPrice = view.findViewById(R.id.textViewPrice);
        mTvPrice.setText("Giá: " + mPizza.getPrice() + "đ");

        // set count pizza order
        mTvCount = view.findViewById(R.id.textViewCount);
        mTvCount.setText("1");

        // set event click of two button
        setButtonClickListener(view);
    }

    @SuppressLint("SetTextI18n")
    private void setButtonClickListener(View view) {
        Button mBtAdd = view.findViewById(R.id.buttonAdd);
        Button mBtSub = view.findViewById(R.id.buttonSub);
        mBtAdd.setOnClickListener(v -> {
            ++count;
            mTvCount.setText(String.valueOf(count));
            int price = Integer.parseInt(mPizza.getPrice());
            price = price * count;
            mTvPrice.setText("Giá: " + price + "đ");
        });

        mBtSub.setOnClickListener(v -> {
            --count;
            if (count < 1) count = 1;
            mTvCount.setText(String.valueOf(count));
            int price = Integer.parseInt(mPizza.getPrice());
            price = price * count;
            mTvPrice.setText("Giá: " + price + "đ");
        });

        ImageButton imgBtBack = view.findViewById(R.id.imageButtonBack);
        imgBtBack.setOnClickListener(v -> getActivity().getFragmentManager().popBackStack());

        Button btOrder = view.findViewById(R.id.buttonOrder);
        btOrder.setOnClickListener(v -> {
            if (mUser == null) handleNotUser();
            else confirmOrder();
        });
        Button btAddCart = view.findViewById(R.id.buttonThemGioHang);

        btAddCart.setOnClickListener(v -> {
            if (mUser == null) handleNotUser();
            else {
                String status = "Chưa order";
                orderPizza(status, 1);
            }
        });

        ImageView imgViewMessage = view.findViewById(R.id.imageViewMessage);
        imgViewMessage.setOnClickListener(v -> loadFragment(new MessageFragment()));
    }

    private void handleNotUser() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle("Thông báo");
        builder.setMessage("Bạn phải đăng nhập để thực hiện chức năng này!");
        builder.setPositiveButton("Đồng ý", (dialog, which) -> {
            loadFragment(new LoginFragment());
        });
        builder.setNegativeButton("Hủy", (dialog, which) -> {
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void orderPizza(String status, int type) {
        int idUser = mUser.getId();
        int idPizza = mPizza.getId();
        String amount = mTvCount.getText().toString();
        String price = String.valueOf(Integer.parseInt(amount) * Integer.parseInt(mPizza.getPrice()));
        String name = mUser.getName();
        String address = mUser.getAddress();
        String phone = mUser.getPhone();
        JSONObject object = new JSONObject();
        try {
            object.put("id_user", idUser);
            object.put("id_pizza", idPizza);
            object.put("amount", amount);
            object.put("total_price", price);
            object.put("name_order", name);
            object.put("address_order", address);
            object.put("phone_order", phone);
            object.put("status_order", status);
            Log.e("object", object.toString());
            mSocket.emit("add_order", object);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mSocket.on("add_cart", args -> {
            String check = (String) args[0];
            if (check.equals("success")) {
                if(type == 1)
                mActivity.runOnUiThread(() ->
                        Toast.makeText(mActivity, "Thêm vào giỏ hàng thành công!", Toast.LENGTH_SHORT).show());
                if(type == 0) mActivity.runOnUiThread(() ->
                        Toast.makeText(mActivity, "Đặt hàng thành công!", Toast.LENGTH_SHORT).show());
            } else mActivity.runOnUiThread(() ->
                    Toast.makeText(mActivity, "Thất bại!", Toast.LENGTH_SHORT).show());
        });
    }

    private void confirmOrder() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle("Xác nhận thông tin");
        builder.setMessage("Pizza: " + mPizza.getName()
                + "\nSố lương: " + mTvCount.getText().toString()
                + "\n"+ mTvPrice.getText().toString()
                + "\nNgười đặt: "+ mUser.getName()
                + "\nĐịa chỉ: "+ mUser.getAddress()
                + "\nSố điện thoại: "+ mUser.getPhone());
        builder.setNegativeButton("Hủy", (dialog, which) -> {
        });
        builder.setPositiveButton("Xác nhân", (dialog, which) -> {
            String status = "Chờ xử lý";
            orderPizza(status, 0);
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


    private void loadFragment(Fragment frameLayout) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, frameLayout).addToBackStack(null);
        fragmentTransaction.commit(); // save the changes
    }

}