package com.studentproject.orderpizza.view.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Bundle;

import android.provider.Settings.Secure;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.studentproject.orderpizza.MainActivity;
import com.studentproject.orderpizza.R;
import com.studentproject.orderpizza.adapter.ListMessageAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.UUID;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.socket.client.IO;
import io.socket.client.Socket;

public class MessageFragment extends Fragment {

    private Socket mSocket;

    {
        try {
            mSocket = IO.socket("https://orderpizza-node-api.herokuapp.com/");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private static final String uniqueID = UUID.randomUUID().toString();

    private MainActivity mActivity;
    private ArrayList<String> mMessages;
    private ListMessageAdapter mListMessageAdapter;

    public MessageFragment() {
        mMessages = new ArrayList<>();
    }

    public static MessageFragment newInstance(String param1, String param2) {
        MessageFragment fragment = new MessageFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();
        mSocket.connect();
        receiverMessage();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        EditText edtMessage = view.findViewById(R.id.editTextMessage);
        edtMessage.setOnClickListener(v -> {
            sendMessageToSever(edtMessage.getText().toString());
            edtMessage.setText("");
        });

        ImageButton imgBtSend = view.findViewById(R.id.imageButtonSend);
        imgBtSend.setOnClickListener(v -> {
            sendMessageToSever(edtMessage.getText().toString());
            edtMessage.setText("");
        });

        ImageButton imgBtBack = view.findViewById(R.id.imageButtonBack);
        imgBtBack.setOnClickListener(v -> mActivity.getFragmentManager().popBackStack());

        RecyclerView recyclerView = view.findViewById(R.id.recycleViewMessage);
        mListMessageAdapter = new ListMessageAdapter(mActivity, mMessages);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setAdapter(mListMessageAdapter);

    }


    private void sendMessageToSever(String message) {
        mSocket.emit("user_message", message);
        mActivity.runOnUiThread(() -> {
            if (!message.equals("")) mMessages.add("customer: " + message);
            mListMessageAdapter.notifyDataSetChanged();
        });

    }
    private void receiverMessage(){
        mSocket.on("receiver_message_admin", args -> {
            String messageAdmin = (String) args[0];
            mActivity.runOnUiThread(() -> {
                if (!messageAdmin.equals("")) mMessages.add("admin: " + messageAdmin);
                mListMessageAdapter.notifyDataSetChanged();
            });

        });
    }
}