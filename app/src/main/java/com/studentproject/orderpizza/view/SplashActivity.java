package com.studentproject.orderpizza.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.studentproject.orderpizza.MainActivity;

public class SplashActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startActivity(new Intent(SplashActivity.this, MainActivity.class));
        finish();
    }
}
