package com.studentproject.orderpizza.view.fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.os.Bundle;


import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.studentproject.orderpizza.MainActivity;
import com.studentproject.orderpizza.R;
import com.studentproject.orderpizza.adapter.ListCartAdapter;
import com.studentproject.orderpizza.adapter.ListItemClickListener;
import com.studentproject.orderpizza.model.Pizza;
import com.studentproject.orderpizza.model.PizzaOrder;

import org.json.JSONArray;

import java.net.URISyntaxException;
import java.util.ArrayList;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.socket.client.IO;
import io.socket.client.Socket;

public class CartFragment extends Fragment implements ListItemClickListener {

    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("https://orderpizza-node-api.herokuapp.com/");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
    private ListCartAdapter mListCartAdapter;
    private ArrayList<PizzaOrder> pizzaOrders;
    private MainActivity mActivity;

    public CartFragment() {
        pizzaOrders = new ArrayList<>();
    }

    public static CartFragment newInstance(String param1, String param2) {
        CartFragment fragment = new CartFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();
        if (getArguments() != null) {
            pizzaOrders = getArguments().getParcelableArrayList("cart");
        }
        updateStatus();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cart, container, false);
        mSocket.connect();
        initView(view);
        return view;
    }

    private void initView(View view) {
        ImageButton imgBtBack = view.findViewById(R.id.imageButtonBack);
        imgBtBack.setOnClickListener(v -> {
            mActivity.getFragmentManager().popBackStack();
        });
        RecyclerView recyclerView = view.findViewById(R.id.recyclerviewListCard);
        mListCartAdapter = new ListCartAdapter(mActivity, pizzaOrders, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setAdapter(mListCartAdapter);
    }



    @Override
    public void onListItemClick(int position, boolean isLongClick) {
        if(isLongClick) confirmDeleteOrder(pizzaOrders.get(position));
    }

    private void huyOrder(PizzaOrder pizzaOrder) {
        mSocket.emit("delete_cart", pizzaOrder.getId());
        mSocket.on("status_delete", args -> {
            String status = (String) args[0];
            if (status.equals("success")) {
                mActivity.runOnUiThread(() -> {
                    Toast.makeText(mActivity, "Hủy đơn hàng thành công!", Toast.LENGTH_SHORT).show();
                    pizzaOrders.remove(pizzaOrder);
                    mListCartAdapter.notifyDataSetChanged();
                });
            } else mActivity.runOnUiThread(() ->
                    Toast.makeText(mActivity, "Hủy đơn hàng thất bại!", Toast.LENGTH_SHORT).show());
        });

    }

    private void confirmDeleteOrder(PizzaOrder pizzaOrder) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle("Xác nhận");
        builder.setMessage("Bạn chắc chắn muốn xóa đơn hàng này!");
        builder.setPositiveButton("Đồng ý", (dialog, which) -> {
            huyOrder(pizzaOrder);
        });
        builder.setNegativeButton("Hủy", (dialog, which) -> {
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void getListCart() {
        mSocket.connect();
        mSocket.emit("get_cart", MainActivity.getmUser().getId());
        mSocket.on("list_cart", args -> {
            pizzaOrders.clear();
            JSONArray jsonArray = (JSONArray) args[0];
            pizzaOrders = PizzaOrder.fromJson(jsonArray);

        });
    }
    private void updateStatus() {
        mSocket.connect();
        mSocket.on("status_update_order", args -> {
            String statusUpdate = (String) args[0];
            if (statusUpdate.equals("success")) {
                mActivity.runOnUiThread(this::getListCart);
            }

        });
    }

}