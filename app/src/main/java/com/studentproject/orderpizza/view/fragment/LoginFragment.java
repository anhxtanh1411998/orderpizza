package com.studentproject.orderpizza.view.fragment;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;

import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.studentproject.orderpizza.MainActivity;
import com.studentproject.orderpizza.R;
import com.studentproject.orderpizza.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.concurrent.atomic.AtomicReference;

import io.socket.client.IO;
import io.socket.client.Socket;


public class LoginFragment extends Fragment {
    private User mUser;
    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("https://orderpizza-node-api.herokuapp.com/");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private MainActivity mActivity;
    private EditText mUsername, mPassword;
    private Button nBtLogin;
    private TextView mTvSignIn;
    private ImageButton mImgBtBack;

    private SendData mListener;

    public interface SendData {
        void imLeavingYou(User user);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (SendData) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mListener != null) {
            mListener.imLeavingYou(mUser);
        }
    }

    public LoginFragment() {
        mUser = new User();
    }

    public static LoginFragment newInstance(String param1, String param2) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSocket.connect();
        mActivity = (MainActivity) this.getActivity();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        mUsername = view.findViewById(R.id.editTextUser);
        mPassword = view.findViewById(R.id.editTextPassword);
        nBtLogin = view.findViewById(R.id.buttonLogin);
        mTvSignIn = view.findViewById(R.id.textViewSignIn);
        mImgBtBack = view.findViewById(R.id.imageButtonBack);

        //set event
        setEventClick();
    }


    private void setEventClick() {
        // Login click
        nBtLogin.setOnClickListener(v -> {
            requestLogin();
            login();
        });

        // sign in click
        mTvSignIn.setOnClickListener(v -> loadFragment(new SignInFragment()));

        // image button back click
        mImgBtBack.setOnClickListener(v -> {
            mUser = null;
            mActivity.getFragmentManager().popBackStack();
        });
    }

    private void loadFragment(Fragment frameLayout) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, frameLayout).addToBackStack(null);
        fragmentTransaction.commit(); // save the changes
    }

    private void requestLogin() {
        String username = mUsername.getText().toString();
        String password = mPassword.getText().toString();
        JSONObject user = new JSONObject();
        try {
            user.put("role", 1);
            user.put("username", username);
            user.put("password", password);
            mSocket.emit("login", user);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void login() {
        mSocket.on("data_user", args -> {
            JSONObject obj = (JSONObject) args[0];
            try {
                if (obj.getJSONArray("rows").length() != 0) {
                    JSONArray jsonArray = obj.getJSONArray("rows");
                    mUser = User.fromJson(jsonArray.getJSONObject(0));
                    MainActivity.mUser = mUser;
                    mActivity.getFragmentManager().popBackStack();
                } else {
                    mActivity.runOnUiThread(() -> {
                        Toast.makeText(mActivity, "Sai tên đăng nhập hoặc mật khẩu!", Toast.LENGTH_SHORT).show();
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onDestroy() {
        mSocket.disconnect();
        super.onDestroy();
    }
}